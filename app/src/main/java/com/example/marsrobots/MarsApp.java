package com.example.marsrobots;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.marsrobots.data.repository.GalleryItemRepository;
import com.example.marsrobots.data.AppDatabase;

public class MarsApp extends Application {

    private static MarsApp INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }

    public static MarsApp getInstance(){
        return INSTANCE;
    }


    public GalleryItemRepository getGalleryItemsRepository() {
        return GalleryItemRepository.getInstance(AppDatabase.getInstance(this));
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
