package com.example.marsrobots.adapter;

import android.view.View;
import android.widget.ProgressBar;

import com.example.marsrobots.R;

import androidx.recyclerview.widget.RecyclerView;

public class LoaderViewHolder extends RecyclerView.ViewHolder {

    ProgressBar mProgressBar;

    LoaderViewHolder(View itemView) {
        super(itemView);
        mProgressBar = itemView.findViewById(R.id.progressbar);
    }
}
