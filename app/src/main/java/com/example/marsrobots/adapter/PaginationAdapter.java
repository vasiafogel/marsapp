package com.example.marsrobots.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.marsrobots.R;
import com.example.marsrobots.data.enteties.GalleryItem;

import java.text.SimpleDateFormat;
import java.util.Locale;

import androidx.recyclerview.widget.RecyclerView;

public class PaginationAdapter extends FooterLoaderAdapter<GalleryItem> {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.UK);

    private Context context;

    public PaginationAdapter(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        return new PaginationViewHolder(mInflater.inflate(R.layout.item_gallery, parent, false));
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PaginationViewHolder) {
            PaginationViewHolder viewHolder = (PaginationViewHolder) holder;
            final GalleryItem item = mItems.get(position);
            viewHolder.description.setText(item.getDescription());
            viewHolder.date.setText(simpleDateFormat.format(item.getDate()));


            Glide
                    .with(context)
                    .load(item.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(((PaginationViewHolder) holder).image);
        }
    }

    public class PaginationViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView description;
        private TextView date;

        PaginationViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.iv_image);
            description = itemView.findViewById(R.id.tv_description);
            date = itemView.findViewById(R.id.tv_date);
        }
    }


}

