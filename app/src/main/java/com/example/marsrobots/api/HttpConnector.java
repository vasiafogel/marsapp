package com.example.marsrobots.api;

import com.example.marsrobots.api.services.AppService;
import com.example.marsrobots.utils.Constants;
import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpConnector {

    private final Retrofit retrofit;
    private static HttpConnector instance;

    private HttpConnector() {


        final OkHttpClient tppHttpClient = new OkHttpClient().newBuilder()
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(tppHttpClient)
                .build();
    }

    public static synchronized HttpConnector getInstance() {
        if (instance == null) {
            instance = new HttpConnector();
        }
        return instance;
    }

    public AppService getAppService() {
        return retrofit.create(AppService.class);
    }

}
