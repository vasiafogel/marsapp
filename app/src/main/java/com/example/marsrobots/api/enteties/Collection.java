package com.example.marsrobots.api.enteties;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Collection implements Parcelable {

    @SerializedName("items")
    private ArrayList<Item> items;
    @SerializedName("links")
    private ArrayList<Link> links;

    protected Collection(Parcel in) {
        items = in.createTypedArrayList(Item.CREATOR);
        links = in.createTypedArrayList(Link.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(items);
        dest.writeTypedList(links);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Collection> CREATOR = new Creator<Collection>() {
        @Override
        public Collection createFromParcel(Parcel in) {
            return new Collection(in);
        }

        @Override
        public Collection[] newArray(int size) {
            return new Collection[size];
        }
    };

    public ArrayList<Item> getItems() {
        return items;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }
}
