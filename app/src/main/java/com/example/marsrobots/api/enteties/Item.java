package com.example.marsrobots.api.enteties;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Item implements Parcelable {

    @SerializedName("data")
    private ArrayList<Data> data;
    @SerializedName("links")
    private ArrayList<Link> links;

    protected Item(Parcel in) {
        data = in.createTypedArrayList(Data.CREATOR);
        links = in.createTypedArrayList(Link.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
        dest.writeTypedList(links);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public ArrayList<Data> getData() {
        return data;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }
}
