package com.example.marsrobots.api.enteties;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Link implements Parcelable {

    @SerializedName("href")
    private String href;
    @SerializedName("prompt")
    private String prompt;
    @SerializedName("rel")
    private String rel;

    protected Link(Parcel in) {
        href = in.readString();
        prompt = in.readString();
        rel = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(href);
        dest.writeString(prompt);
        dest.writeString(rel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Link> CREATOR = new Creator<Link>() {
        @Override
        public Link createFromParcel(Parcel in) {
            return new Link(in);
        }

        @Override
        public Link[] newArray(int size) {
            return new Link[size];
        }
    };

    public String getHref() {
        return href;
    }

    public String getPrompt() {
        return prompt;
    }

    public String getRel() {
        return rel;
    }
}
