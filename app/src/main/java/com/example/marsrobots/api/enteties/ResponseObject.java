package com.example.marsrobots.api.enteties;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ResponseObject implements Parcelable {

    @SerializedName("collection")
    private Collection collection;

    protected ResponseObject(Parcel in) {
        collection = in.readParcelable(Collection.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(collection, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResponseObject> CREATOR = new Creator<ResponseObject>() {
        @Override
        public ResponseObject createFromParcel(Parcel in) {
            return new ResponseObject(in);
        }

        @Override
        public ResponseObject[] newArray(int size) {
            return new ResponseObject[size];
        }
    };

    public Collection getCollection() {
        return collection;
    }

}
