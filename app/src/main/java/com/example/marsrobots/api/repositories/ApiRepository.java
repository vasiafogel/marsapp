package com.example.marsrobots.api.repositories;

import com.example.marsrobots.MarsApp;
import com.example.marsrobots.api.HttpConnector;
import com.example.marsrobots.api.enteties.Item;
import com.example.marsrobots.api.enteties.ResponseObject;
import com.example.marsrobots.data.enteties.GalleryItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApiRepository {

    public static Observable<List<GalleryItem>> getList(int page){
        return HttpConnector.getInstance()
                .getAppService()
                .getItems("mars", "image", page)
                .flatMap(responseObject -> Observable.just(covertResponse(responseObject, page)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private static List<GalleryItem> covertResponse(ResponseObject response, int page) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.UK);
        List<GalleryItem> list = new ArrayList<>();
        for (Item item : response.getCollection().getItems()){
            GalleryItem galleryItem = new GalleryItem();
            galleryItem.setDescription(item.getData().get(0).getDescription());
            galleryItem.setDate(simpleDateFormat.parse(item.getData().get(0).getDate()));
            galleryItem.setImage(item.getLinks().get(0).getHref());
            galleryItem.setId(item.getData().get(0).getId());
            galleryItem.setPosition(page*100 + response.getCollection().getItems().indexOf(item));
            list.add(galleryItem);
        }
        MarsApp.getInstance().getGalleryItemsRepository().insertAll(list);
        return list;
    }


}
