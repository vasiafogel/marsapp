package com.example.marsrobots.api.services;

import com.example.marsrobots.api.enteties.ResponseObject;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AppService {

    @GET("search")
    Observable<ResponseObject> getItems(
            @Query("q") String searchText,
            @Query("media_type") String mediaType,
            @Query("page") int page);

}
