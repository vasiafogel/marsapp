package com.example.marsrobots.core;

/**
 * Created by laszlo on 20/02/2018.
 */

public interface BasePresenter<V> {

    /**
     * Called when the view is attached to the presenter.
     *
     * @param view the view.
     */
    void onViewAttached(V view);

    /**
     * Called when the view is detached from the presenter.
     */
    void onViewDetached();

    /**
     * Called every time the view starts.
     * The view is guarantee to be not null starting at this method, until [onStop] is called.
     *
     * @param firstStart true if it's the first start of the presenter, only once in the presenter lifetime.
     */
    void onStart(Boolean firstStart);

    /**
     * Called every time the view stops.
     * After this method, the view will be null until next [onStart] call.
     */
    void onStop();

    /**
     * Called when the presenter is definitely destroyed.
     * Use this method only to release any resource used by the presenter (cancel HTTP requests, close database connections, etc.).
     */
    void onPresenterDestroyed();

}
