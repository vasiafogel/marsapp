package com.example.marsrobots.core;


/**
 * Created by laszlo on 20/02/2018.
 */

public abstract class BasePresenterImpl<V extends BaseView> implements BasePresenter<V> {

    protected V view = null;

    @Override
    public void onViewAttached(V view) {
        this.view = view;
    }

    @Override
    public void onViewDetached() {
    }

}