package com.example.marsrobots.core;

import android.os.Bundle;
import android.widget.Toast;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

public abstract class BaseRetainActivity<P extends BasePresenter<V>, V> extends AppCompatActivity implements BaseView, LoaderManager.LoaderCallbacks<P> {

    public static  final String FIRST_START = "FIRST_START";
    public static  final String LOADER_ID_KEY = "LOADER_ID_KEY";
    public static  final AtomicInteger LOADER_ID_VALUE = new AtomicInteger(0);

    protected P presenter = null;

    /**
     * Unique identifier for the [Loader], persisted across configuration changes.
     */
    private int loaderId = 0;

    /**
     * True if this is the first time the activity is created.
     * Used to avoid unnecessary calls after activity recreation.
     */
    private boolean firstStart = true;

    /**
     * True if presenter is null (not loaded yet) when [onStart] is called.
     * Used to make sure presenter is available before start working.
     */
    private AtomicBoolean needToCallStart = new AtomicBoolean(false);

    public abstract PresenterFactory<P> presenterFactory();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            firstStart = true;
            loaderId = LOADER_ID_VALUE.getAndIncrement();
        } else {
            firstStart = savedInstanceState.getBoolean(FIRST_START);
            loaderId = savedInstanceState.getInt(LOADER_ID_KEY);
        }
        getSupportLoaderManager().initLoader(loaderId, Bundle.EMPTY, this).startLoading();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (presenter == null) {
            needToCallStart.set(true);
        } else {
            doStart();
        }
    }

    private void doStart() {
        presenter.onViewAttached((V) this);
        presenter.onStart(firstStart);

        firstStart = false;
    }

    @Override
    public void onStop() {
        if (presenter != null) {
            presenter.onStop();
            presenter.onViewDetached();
        }
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FIRST_START, firstStart);
        outState.putInt(LOADER_ID_KEY, loaderId);
    }

    // LoaderCallbacks

    @Override
    public Loader<P> onCreateLoader(int id, Bundle args) {
        PresenterLoader<P> presenterLoader = new PresenterLoader<>(this);
        presenterLoader.setFactory(presenterFactory());
        return presenterLoader;
    }

    @Override
    public void onLoadFinished(Loader<P> loader, P data) {
        presenter = data;
        if (needToCallStart.compareAndSet(true, false)) {
            doStart();
        }
    }

    @Override
    public void onLoaderReset(Loader<P> loader) {
        presenter = null;
    }

    public void showMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

}
