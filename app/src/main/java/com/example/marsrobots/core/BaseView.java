package com.example.marsrobots.core;

/**
 * Created by laszlo on 20/02/2018.
 */

public interface BaseView {

    void showMessage(String message);

}
