package com.example.marsrobots.core;

import android.content.Context;

import androidx.loader.content.Loader;


/**
 * Created by laszlo on 20/02/2018.
 */

public class PresenterLoader<P extends BasePresenter> extends Loader<P> {

    private PresenterFactory<P> factory;
    private P presenter = null;

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context used to retrieve the application context.
     */
    public PresenterLoader(Context context) {
        super(context);
    }

    public void setFactory(PresenterFactory<P> factory){
        this.factory = factory;
    }

    @Override
    public void onStartLoading() {
        if (presenter != null) {
            deliverResult(presenter);
            return;
        }
        forceLoad();
    }

    @Override
    public void onForceLoad() {
        presenter = factory.create();
        deliverResult(presenter);
    }

    @Override
    public void onReset() {
        if (presenter != null) {
            presenter.onPresenterDestroyed();
            presenter = null;
        }
    }

}
