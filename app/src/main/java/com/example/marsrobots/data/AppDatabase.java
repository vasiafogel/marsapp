package com.example.marsrobots.data;

import android.content.Context;

import com.example.marsrobots.data.converters.DateConverter;
import com.example.marsrobots.data.dao.GalleryItemDao;
import com.example.marsrobots.data.enteties.GalleryItem;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {GalleryItem.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase mInstance;

    public abstract GalleryItemDao galleryItemDao();

    public static AppDatabase getInstance(final Context context) {
        if (mInstance == null) {
            synchronized (AppDatabase.class) {
                if (mInstance == null) {
                    mInstance = Room.databaseBuilder(context, AppDatabase.class, "mars").build();
                }
            }
        }
        return mInstance;
    }
}
