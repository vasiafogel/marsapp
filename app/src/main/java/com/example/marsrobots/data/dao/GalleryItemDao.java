package com.example.marsrobots.data.dao;

import com.example.marsrobots.data.enteties.GalleryItem;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Observable;

@Dao
public interface GalleryItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<GalleryItem> items);

    @Query("SELECT * from gallery_items ORDER BY position LIMIT :startIndex,:endIndex")
    Observable<List<GalleryItem>> selectGalleryItems(int startIndex, int endIndex);

}
