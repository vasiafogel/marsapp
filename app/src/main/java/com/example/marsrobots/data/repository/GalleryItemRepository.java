package com.example.marsrobots.data.repository;

import android.os.AsyncTask;

import com.example.marsrobots.data.AppDatabase;
import com.example.marsrobots.data.enteties.GalleryItem;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GalleryItemRepository {

    private static GalleryItemRepository INSTANCE;
    private final AppDatabase appDatabase;

    public static GalleryItemRepository getInstance(final AppDatabase appDatabase){
        if (INSTANCE == null){
            synchronized (GalleryItemRepository.class){
                if (INSTANCE == null){
                    INSTANCE = new GalleryItemRepository(appDatabase);
                }
            }
        }
        return INSTANCE;
    }

    private GalleryItemRepository(final AppDatabase appDatabase){
        this.appDatabase = appDatabase;
    }

    public void insertAll(final List<GalleryItem> list){
        AsyncTask.execute(() -> appDatabase.galleryItemDao().insertAll(list));
    }

    public Observable<List<GalleryItem>> getList(int page){
        int startIndex = (page-1)*100;
        int endIndex = page*100;
        return appDatabase.galleryItemDao().selectGalleryItems(startIndex, endIndex)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
