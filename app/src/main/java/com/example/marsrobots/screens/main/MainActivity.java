package com.example.marsrobots.screens.main;


import android.os.Bundle;

import com.example.marsrobots.R;
import com.example.marsrobots.adapter.PaginationAdapter;
import com.example.marsrobots.adapter.RecyclerViewScrollListener;
import com.example.marsrobots.core.BaseRetainActivity;
import com.example.marsrobots.core.PresenterFactory;
import com.example.marsrobots.data.enteties.GalleryItem;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends BaseRetainActivity<MainPresenter, MainView> implements MainView{

    private RecyclerView rvGalleryItems;
    private PaginationAdapter adapter;
    private RecyclerViewScrollListener scrollListener;

    @Override
    public PresenterFactory<MainPresenter> presenterFactory() {
        return () -> new MainPresenterImpl(new MainModelImpl());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO: need to handle screen orientation change
        setContentView(R.layout.activity_main);
        rvGalleryItems = findViewById(R.id.rv_gallery_item);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        rvGalleryItems.setLayoutManager(layoutManager);
        adapter = new PaginationAdapter(this);
        rvGalleryItems.setAdapter(adapter);
        scrollListener = new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                presenter.loadNextPage();
            }
        };
        rvGalleryItems.setOnScrollListener(scrollListener);
    }


    @Override
    public void setData(List<GalleryItem> list) {
        adapter.setItems(new ArrayList<>(list));
    }

    @Override
    public void onError() {
        scrollListener.setNotLoading();
    }
}
