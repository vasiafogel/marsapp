package com.example.marsrobots.screens.main;

import com.example.marsrobots.data.enteties.GalleryItem;

import java.util.List;

public interface MainModel {

    interface OnGalleryItemsSearchListener {
        void onSuccess(List<GalleryItem> list);
        void onFailure(String errorMessage);
    }

    void getGalleryItemList(OnGalleryItemsSearchListener listener, int page);
    void dispose();

}
