package com.example.marsrobots.screens.main;

import com.example.marsrobots.MarsApp;
import com.example.marsrobots.api.repositories.ApiRepository;

import io.reactivex.disposables.CompositeDisposable;

public class MainModelImpl implements MainModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void getGalleryItemList(OnGalleryItemsSearchListener listener, int page) {
        if (MarsApp.getInstance().isNetworkAvailable()) {
            compositeDisposable.add(ApiRepository.getList(page)
                    .subscribe(listener::onSuccess, error -> listener.onFailure(error.getMessage())));
        } else {
            compositeDisposable.add(MarsApp.getInstance().getGalleryItemsRepository().getList(page)
            .subscribe(listener::onSuccess, error -> listener.onFailure(error.getMessage())));
        }
    }

    @Override
    public void dispose() {
        if (compositeDisposable != null){
            compositeDisposable.dispose();
        }
    }
}
