package com.example.marsrobots.screens.main;

import com.example.marsrobots.core.BasePresenter;

public interface MainPresenter extends BasePresenter<MainView> {

    void loadNextPage();

}
