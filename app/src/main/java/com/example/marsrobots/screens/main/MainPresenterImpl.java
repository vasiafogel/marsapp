package com.example.marsrobots.screens.main;

import com.example.marsrobots.core.BasePresenterImpl;
import com.example.marsrobots.data.enteties.GalleryItem;

import java.util.List;

public class MainPresenterImpl extends BasePresenterImpl<MainView>
        implements MainModel.OnGalleryItemsSearchListener, MainPresenter {


    private final MainModel mainModel;
    private int page;

    public MainPresenterImpl(MainModel mainModel){
        this.mainModel = mainModel;
        page = 1;
    }

    @Override
    public void onStart(Boolean firstStart) {
        if (firstStart){
            mainModel.getGalleryItemList(this, page);
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPresenterDestroyed() {
        mainModel.dispose();
    }

    @Override
    public void onSuccess(List<GalleryItem> list) {
        if (list != null && list.size() > 0) {
            view.setData(list);
            page++;
        } else {
            view.onError();
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        view.showMessage(errorMessage);
        view.onError();
    }

    @Override
    public void loadNextPage() {
        mainModel.getGalleryItemList(this, page);
    }
}
