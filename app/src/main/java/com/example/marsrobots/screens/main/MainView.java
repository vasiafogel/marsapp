package com.example.marsrobots.screens.main;

import com.example.marsrobots.core.BaseView;
import com.example.marsrobots.data.enteties.GalleryItem;

import java.util.List;

public interface MainView extends BaseView {

    void setData(List<GalleryItem> list);
    void onError();

}
